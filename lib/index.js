const opn = require('opn');
let express = require("express");
var myParser = require("body-parser");
let app = express();

app.use(myParser.json());
app.use(myParser.urlencoded({extended: true}));
app.use(express.static(__dirname + '/server/files'));

const sites = require('./server/sites.js');

var startServer = function(port) {

	sites(app);

	app.listen(port, 'localhost', () => {
		"use strict";

		opn(`http://localhost:${port}`);
		console.log(`Server Running on ${port}`);
	});
};

// Allows us to call this function from outside of the library file.
// Without this, the function would be private to this file.
exports.startServer = startServer;