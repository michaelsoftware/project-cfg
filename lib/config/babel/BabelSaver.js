const path = require('path');
const FileHelper = require('../../utils/FileHelper');
const PackageInstaller = require('../PackageInstaller');
const CONFIG_PATH = path.join(process.cwd(), '.babelrc');
const PACKAGES = require('../../../const/packages');

const BabelLoader = require('./BabelLoader');


module.exports = class BabelSaver {

	constructor() {
		this.babelLoader = new BabelLoader();
	}

	save(config, globalConfig) {
		if(config.enabled)
			return this.babelLoader.load(false).then((data) => {

				delete data.enabled;

				PackageInstaller.install(PACKAGES.BABEL.core);


				if(!data.presets) {
					data.presets = [];
				}
				if(!data.plugins) {
					data.plugins = [];
				}

				managePackage(data.presets, 'es2015', config.es6enabled);
				managePackage(data.presets, 'react', config.reactEnabled);

				managePackage(data.presets, 'stage-0', config.stage === 'stage-0');
				managePackage(data.presets, 'stage-1', config.stage === 'stage-1');
				managePackage(data.presets, 'stage-2', config.stage === 'stage-2');
				managePackage(data.presets, 'stage-3', config.stage === 'stage-3');

				managePackage(data.plugins, 'transform-decorators-legacy', config.decoratorsEnabled);
				managePackage(data.plugins, 'transform-class-properties',  config.classPropertiesEnabled);
				managePackage(data.plugins, 'transform-runtime', config.runtimeEnabled);


				return PackageInstaller.apply().then(() => {
					return this.writeConfig(data);
				});
			});
		else {
			PackageInstaller.remove(PACKAGES.BABEL.core);
			return this.writeConfig({});
		}
	}

	writeConfig(data) {
		return FileHelper.save(CONFIG_PATH, JSON.stringify(data, null, "\t"));
	}
};


function managePackage(presets, name, enabled) {
	if(enabled) {
		PackageInstaller.install(PACKAGES.BABEL[name]);
		addToArray(presets, name);
	} else {
		PackageInstaller.remove(PACKAGES.BABEL[name]);
		removeFromArray(presets, name);
	}
}

function addToArray(array, entry) {
	if(array.indexOf(entry) === -1) {
		array.push(entry);
	}
}

function removeFromArray(array, entry) {
	let index = array.indexOf(entry);
	if(index !== -1) {
		array.splice(index, 1);
	}
}