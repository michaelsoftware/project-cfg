const path = require('path');
const FileHelper = require('../../utils/FileHelper');
const CONFIG_PATH = path.join(process.cwd(), '.babelrc');

module.exports = class BabelLoader {
	load(parse) {
		return FileHelper.load(CONFIG_PATH).then((data) => {
			return JSON.parse(data.toString());
		}).then((config) => {
			if(parse === false) return config;

			return BabelLoader.parse(config);
		}).catch(() => {
			return {
				enabled: false
			};
		});
	}

	static parse(config) {
		let retval = {
			enabled: !(Object.keys(config).length === 0 && config.constructor === Object) // !empty object
		};

		if(config.presets) {
			retval.es6enabled = ( config.presets.indexOf('es2015') !== -1 );
			retval.reactEnabled = ( config.presets.indexOf('react') !== -1 );
			retval.stage = BabelLoader.getStage(config.presets);
		}

		if(config.plugins) {
			retval.decoratorsEnabled = ( config.plugins.indexOf('transform-decorators-legacy') !== -1 );
			retval.classPropertiesEnabled = ( config.plugins.indexOf('transform-class-properties') !== -1 );
			retval.runtimeEnabled = ( config.plugins.indexOf('transform-runtime') !== -1 );
		}

		return retval;
	}

	static getStage(presets) {
		if(presets.indexOf('stage-0') !== -1) return 'stage-0';
		if(presets.indexOf('stage-1') !== -1) return 'stage-1';
		if(presets.indexOf('stage-2') !== -1) return 'stage-2';
		if(presets.indexOf('stage-3') !== -1) return 'stage-3';

		return '0';
	}
};