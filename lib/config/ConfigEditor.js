const path = require('path');

const FileHelper = require('../utils/FileHelper');
const CONFIG_PATH = path.join(process.cwd(), 'project.cfg');

const NpmLoader = require('./npm/NpmLoader');
const BabelLoader = require('./babel/BabelLoader');
const WebpackLoader = require('./webpack/WebpackLoader');

const NpmSaver = require('./npm/NpmSaver');
const BabelSaver = require('./babel/BabelSaver');
const WebpackSaver = require('./webpack/WebpackSaver');


module.exports = class ConfigEditor {
	constructor() {
		this.npmLoader = new NpmLoader();
		this.babelLoader = new BabelLoader();
		this.webpackLoader = new WebpackLoader();

		this.npmSaver = new NpmSaver();
		this.babelSaver = new BabelSaver();
		this.webpackSaver = new WebpackSaver();
	}

	load() {
		"use strict";

		var output = {};


		return this.npmLoader.load().then((npm) => {
			output.npm = npm;
		}).then(() => {
			return this.babelLoader.load();
		}).then((babeljs) => {
			return output.babel = babeljs;
		}).then(()=> {
			return this.webpackLoader.load();
		}).then((webpack) => {
			return output.webpack = webpack;
		}).then(() => {
			return output;
		}).catch((error) => {
			return Promise.resolve({
			});
		});
	}

	save(config) {
		return this.npmSaver.save(config.npm, config).then(() => {
			return this.babelSaver.save( config.babel, config );
		}).then(() => {
			return this.webpackSaver.save( config.webpack, config );
		}).then(() => {
			return FileHelper.save(CONFIG_PATH, JSON.stringify(config, null, "\t"));
		}).then(() => {
			return config;
		});
	}
};