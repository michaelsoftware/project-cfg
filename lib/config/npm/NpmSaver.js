const path = require('path');
const FileHelper = require('../../utils/FileHelper');
const normalize = require('../../utils/normalize');
const CONFIG_PATH = path.join(process.cwd(), 'package.json');

module.exports = class NpmSaver {
	save(config, globalConfig) {
		if(!config.name) {
			config.name = normalize.snakeCase( path.basename(process.cwd()) );
		}

		if(!config.license) {
			config.license = 'ISC';
		}

		return FileHelper.save(CONFIG_PATH, JSON.stringify(config, null, "\t"));
	}
};