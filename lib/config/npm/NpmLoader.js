const path = require('path');
const FileHelper = require('../../utils/FileHelper');
const normalize = require('../../utils/normalize');
const CONFIG_PATH = path.join(process.cwd(), 'package.json');

module.exports = class NpmLoader {
	load() {
		return FileHelper.load(CONFIG_PATH).then((data) => {
			return JSON.parse(data.toString());
		}).catch(() => {
			return Promise.resolve({
				name: normalize.snakeCase( path.basename(process.cwd()) ),
				license: 'ISC'
			});
		});
	}
};