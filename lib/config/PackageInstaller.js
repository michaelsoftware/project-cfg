var exec = require('child_process').exec;

var install = [], remove = [];

module.exports = class PackageInstaller {
	static install(packageName) {
		install.push(packageName);
	}

	static remove(packageName) {
		remove.push(packageName);
	}

	static apply() {
		return new Promise((resolve, reject) => {
			if(install.length === 0) return resolve();

			let packageNames = install.join(' ');

			exec(`npm install --save-dev ${packageNames}`, function(error, stdout, stderr) {
				install = [];

				resolve(stdout);
			});
		}).then(() => {

			return new Promise((resolve, reject) => {
				if(remove.length === 0) return resolve();

				let packageNames = remove.join(' ');

				exec(`npm uninstall --save-dev ${packageNames}`, function (error, stdout, stderr) {
					// command output is in stdout

					remove = [];

					resolve(stdout);
				});
			});
		});
	}
};