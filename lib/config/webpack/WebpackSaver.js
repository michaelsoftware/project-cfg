const path = require('path');
const fs = require('fs');
const FileHelper = require('../../utils/FileHelper');
const PackageInstaller = require('../PackageInstaller');
const PACKAGES = require('../../../const/packages');
const LOADERS = require('../../../const/loaders');
const RESOLVE = require('../../../const/resolve');
const OUTPUT = require('../../../const/output');
const StringUtils = require('../../../utils/StringUtils');

module.exports = class WebpackSaver {
	save(config, globalConfig) {
		if(config.enabled) {

			PackageInstaller.install(PACKAGES.WEBPACK.core);

			managePackage('dev-server', config.devServer);
			managePackage('css-loader', config.css || config.sass);
			managePackage('style-loader', config.css || config.sass);
			managePackage('sass-loader', config.sass);
			managePackage('node-sass', config.sass);


			let es6Enabled = globalConfig.babel.enabled && globalConfig.babel.es6enabled;

			return PackageInstaller.apply().then(() => {
				return Promise.resolve();
			}).then(() => {
				return loadWebpackConfigFile('dev', es6Enabled);
			}).then((data) => {
				data = appendConfigData(data, config, es6Enabled);

				return saveWebpackConfigFile('dev', es6Enabled, data);
			}).then(() => {
				return loadWebpackConfigFile('prod', es6Enabled);
			}).then((data) => {
				data = appendConfigData(data, config, es6Enabled, true);

				return saveWebpackConfigFile('prod', es6Enabled, data);
			});
		} else {
			PackageInstaller.remove(PACKAGES.WEBPACK.core);
			return PackageInstaller.apply().then(() => {
				return Promise.resolve({});
			});
		}
	}
};

function appendConfigData(data, config, es6Enabled, production) {
	"use strict";
	let importArray = [];
	if(es6Enabled) {
		importArray.push('import path from \'path\';');
	} else {
		importArray.push('const path = require(\'path\')');
	}
	let importString = importArray.join("\n");

	let loaders = (config.css || config.sass) ? LOADERS.CSS_STYLE + ',' : '';
	loaders += config.sass ? LOADERS.SASS_STYLE + ',' : '';
	loaders += config.jsx ? LOADERS.JSX + ',' : '';
	loaders += es6Enabled ? LOADERS.BABEL + ',' : '';

	loaders = StringUtils.removeLast(loaders);

	let entry = "\n\t\t\t'./src/index.js'\n\t\t";

	let resolve = RESOLVE.JS + ',';
	resolve += config.jsx ? RESOLVE.JSX + ',' : '';
	resolve = StringUtils.removeLast(resolve);

	let output = OUTPUT.DEFAULT;

	data = data.toString().replace(/(\/\/start imports)([\s\S]+?)(\/\/end imports)/gi, `$1\n${importString}\n$3`);
	data = data.toString().replace(/(\/\/start rules)([\s\S]+?)(\/\/end rules)/gi, `$1\n\t\t\t${loaders}\n\t\t\t$3`);
	data = data.toString().replace(/(\/\/start resolve)([\s\S]+?)(\/\/end resolve)/gi, `$1${resolve}$3`);
	data = data.toString().replace(/(\/\/start entries)([\s\S]+?)(\/\/end entries)/gi, `$1${entry}$3`);
	data = data.toString().replace(/(\/\/start output)([\s\S]+?)(\/\/end output)/gi, `$1${output}$3`);

	return data;
}

function loadWebpackConfigFile(type, useEs6) {
	let webpack = path.join(process.cwd(), 'webpack');
	let webpackFile = path.join(webpack, `${type}.webpack.${useEs6 ? 'babel.js' : 'js'}`);

	let templateFile = path.join(path.dirname(path.dirname(path.dirname(__dirname))), 'const', `webpack.${useEs6 ? 'babel.js' : 'js'}`);

	if (!fs.existsSync(webpack)) {
		fs.mkdirSync(webpack);
	}

	return FileHelper.load(webpackFile).catch(() => {
		"use strict";

		return FileHelper.load(templateFile);
	}).catch((data) => {
		"use strict";

		return Promise.resolve(data);
	});
}

function saveWebpackConfigFile(type, useEs6, data) {
	let webpack = path.join(process.cwd(), 'webpack');
	let webpackFile = path.join(webpack, `${type}.webpack.${useEs6 ? 'babel.js' : 'js'}`);

	if (!fs.existsSync(webpack)) {
		fs.mkdirSync(webpack);
	}

	return FileHelper.save(webpackFile, data);
}


function managePackage(name, enabled) {
	if(enabled) {
		PackageInstaller.install(PACKAGES.WEBPACK[name]);
	} else {
		PackageInstaller.remove(PACKAGES.WEBPACK[name]);
	}
}