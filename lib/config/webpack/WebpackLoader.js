const path = require('path');
const FileHelper = require('../../utils/FileHelper');
const CONFIG_PATH = path.join(process.cwd(), 'project.cfg');

module.exports = class WebpackLoader {
	load() {
		return FileHelper.load(CONFIG_PATH).then((data) => {
			return JSON.parse(data.toString());
		}).then((data) => {
			return data.webpack || {};
		}).catch(() => {
			return Promise.resolve({
				enabled: false
			});
		});
	}
};