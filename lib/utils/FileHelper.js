const fs = require('fs');

module.exports = class FileHelper {
	static load(file) {
		"use strict";

		return new Promise((resolve, reject) => {
			fs.readFile(file, function (err, content) {
				if (err) {
					return reject(err);
				}

				return resolve(content);
			});
		});
	}

	static save(file, content) {
		return new Promise((resolve, reject) => {
			fs.writeFile(file, content, function(err) {
				if(err) {
					return reject(err);
				}

				return resolve(content);
			});
		});
	}
};