module.exports = class {
	static snakeCase(input) {
		input = String(input);

		input = input.toLowerCase();

		while(input.startsWith(".") || input.startsWith("_")) {
			input = input.substring(1);
		}

		input = input.replace(/\s/g, '_');

		return input;
	}
};