const path = require('path');

const ConfigEditor = require('../../config/ConfigEditor');
const Normalize = require('../../utils/normalize');

module.exports = function(app) {

	var configEditor = new ConfigEditor();

	app.use(function(req, res, next) {
		"use strict";

		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
		res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

		if ('OPTIONS' == req.method) {
			res.sendStatus(200);
		}
		else {
			next();
		}
	});

	app.get('/api', (request, response) => {
		"use strict";

		response.writeHead(200, {"Content-Type": "application/json"});


		var config = {
			path: process.cwd()
		};

		configEditor.load().then((config)=> {
			"use strict";

			response.write(JSON.stringify({
				npm: config.npm,
				path: process.cwd(),
				babel: config.babel,
				webpack: config.webpack
			}));
		}).catch(() => {
			response.write('{}');
		}).then(() => {
			response.end();
		});
	});

	app.post('/api/save', (request, response) => {
		"use strict";

		response.writeHead(200, {"Content-Type": "application/json"});

		let body = request.body;

		let output = {
			path: process.cwd()
		};

		configEditor.load().then((npmConfig) => {

			npmConfig.npm.name = Normalize.snakeCase(body.npm.name || '');
			npmConfig.npm.author = body.npm.author || '';
			npmConfig.npm.main = body.npm.main || '';
			npmConfig.npm.version = body.npm.version || '1.0.0';
			npmConfig.npm.keywords = body.npm.keywords || '';
			npmConfig.npm.license = body.npm.license || '';
			npmConfig.npm.homepage = body.npm.homepage || '';
			npmConfig.npm.private = body.npm.private !==  false;
			npmConfig.npm.description = body.npm.description || '';

			npmConfig.npm.bugs = body.npm.bugs || '';

			let useEs6 = false;
			if(body.babel.enabled && body.babel.es6enabled) useEs6 = true;

			if(body.webpack.enabled) {
				npmConfig.npm.scripts = npmConfig.npm.scripts || {};

				if(body.webpack.devServer) {
					let webpackDev = `./webpack/dev.webpack.${useEs6 ? 'babel.js' : 'js'}`;

					npmConfig.npm.scripts.start = `webpack-dev-server --colors --progress --config ${webpackDev} --host 0.0.0.0`;
				}

				let webpack = `./webpack/prod.webpack.${useEs6 ? 'babel.js' : 'js'}`;
				npmConfig.npm.scripts.build = `webpack -p --colors --progress --config ${webpack}`;
			}

			npmConfig.babel = body.babel;
			npmConfig.webpack = body.webpack;

			return configEditor.save(npmConfig);
		}).then((config) => {
			return Object.assign(output, config);
		}).then(() => {
			response.write(JSON.stringify(output));
		}).catch((error) => {
			console.warn(error);
			response.write('{"error":true}');
		}).then(() => {
			response.end();
		});


	});
};