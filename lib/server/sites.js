const path = require('path');

const registerApi = require('./api/index.js');

module.exports = function(app) {
	registerApi(app);
};