module.exports = {
	CSS_STYLE:
			`{
				test: /\.css$/,
				use: [{
					loader: 'css-loader'
				},{
					loader: 'style-loader'
				}],
                exclude: /node_modules/
			}`,
	SASS_STYLE:
			`{
				test: /\.scss$/,
				use: [{
					loader: 'css-loader'
				},{
					loader: 'style-loader'
				},{
					loader: 'sass-loader'
				}],
                exclude: /node_modules/
			}`,
	JSX:
			`{
				test: /\.jsx$/,
				use: [{
					loader: 'babel-loader'
				}],
                exclude: /node_modules/
			}`,
	BABEL:
		`{
				test: /\.js$/,
				use: [{
					loader: 'babel-loader'
				}],
                exclude: /node_modules/
			}`
};