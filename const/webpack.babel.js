/*
 * ##################################################################################
 * #################################   IMPORTANT   ##################################
 * ##################################################################################
 * ##  Do not remove the comments (//start ... & //end ...) in the following code  ##
 * ##  They are used by project-cfg to find the right positions to insert code     ##
 * ##  When you remove the comments project-cfg will not find the right positions  ##
 * ##  project-cfg will then probably override your changes.					   ##
 * ##                                                                              ##
 * ##  Don not write own code between a //start and //end comment. It will be      ##
 * ##  overriden. 																   ##
 * ##################################################################################
 */


//start imports
//end imports


export default {
	entry: [
		//start entries
		//end entries
	],
	resolve: {
		extensions: [
			//start resolve
			//end resolve
		]
	},
	output: {
		//start output
		//end output
	},
	module: {
		rules: [
			//start rules
			//end rules
		]
	},
	plugins: [
		//start plugin
		//end plugin
	]
};