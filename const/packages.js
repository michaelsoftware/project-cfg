module.exports = {
	PKG_BABEL: 'babel',
	BABEL: {
		core: 'babel-core',
		es2015: 'babel-preset-es2015',
		react: 'babel-preset-react',
		'stage-0': 'babel-preset-stage-0',
		'stage-1': 'babel-preset-stage-1',
		'stage-2': 'babel-preset-stage-2',
		'stage-3': 'babel-preset-stage-3',
		'transform-decorators-legacy': 'babel-plugin-transform-decorators',
		'transform-class-properties': 'babel-plugin-transform-class-properties',
		'transform-runtime': 'babel-plugin-transform-runtime'
	},
	WEBPACK: {
		core: 'webpack@2',
		'dev-server': 'webpack-dev-server@2',
		'css-loader': 'css-loader',
		'style-loader': 'style-loader',
		'sass-loader': 'sass-loader',
		'node-sass': 'node-sass'
	}
};