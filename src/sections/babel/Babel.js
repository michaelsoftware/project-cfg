import UiHelper from '../../ui/UiHelper';


let enable, options, es6, react, decorators, properties, stage, runtime;

export default class BabelUi {
	constructor() {
		this.initVars();
	}

	initVars() {
		enable = document.querySelector('.project_babeljs');

		options = document.querySelector('.babel__options');
		es6 = document.querySelector('.babeljs_es6');
		react = document.querySelector('.babeljs_react');
		decorators = document.querySelector('.babeljs_decorators');
		properties = document.querySelector('.babeljs_classProperties');
		stage = document.querySelector('.babeljs_stage');
		runtime = document.querySelector('.babeljs_regeneratorRuntime');

		if(enable)
			enable.onchange = this.updateDisplayOptions;

		this.updateDisplayOptions();
	}

	updateDisplayOptions() {
		if(options)
			options.style.display = (enable.checked) ? 'block' : 'none';
	}

	load(config, globalConfig) {
		enable.checked = !!config.enabled;
		es6.checked = !!config.es6enabled;
		react.checked = !!config.reactEnabled;
		decorators.checked = !!config.decoratorsEnabled;
		properties.checked = !!config.classPropertiesEnabled;
		runtime.checked = !!config.runtimeEnabled;

		stage.value = config.stage || 0;


		this.updateDisplayOptions();
	}

	save() {
		return ({
			enabled: enable.checked,
			es6enabled: es6.checked,
			reactEnabled: react.checked,
			decoratorsEnabled: decorators.checked,
			classPropertiesEnabled: properties.checked,
			runtimeEnabled: runtime.checked,
			stage: stage.value
		});
	}

	getUi() {
		let fragment = document.createDocumentFragment();

		fragment.appendChild( UiHelper.getHeadline("Babel.js-Configuration") );
		fragment.appendChild( UiHelper.getCheckbox("Activate babel.js", "project_babeljs") );

		let container = document.createElement('div');
		container.className = 'babel__options';

			container.appendChild( UiHelper.getHorizontalLine() );
			container.appendChild( UiHelper.getCheckbox("Activate ES6-support", "babeljs_es6") );
			container.appendChild( UiHelper.getCheckbox("Activate react/jsx-support", "babeljs_react") );
			container.appendChild( UiHelper.getCheckbox("Activate decorator-support", "babeljs_decorators") );
			container.appendChild( UiHelper.getCheckbox("Activate classProperty-support", "babeljs_classProperties") );
			container.appendChild( UiHelper.getCheckbox("Activate regenerator-runtime", "babeljs_regeneratorRuntime") );

			container.appendChild( UiHelper.getKeyValueSelect("Stage-preset:", [
				["no preset"],
				["stage-0"],
				["stage-1"],
				["stage-2"],
				["stage-3"],
			], "babeljs_stage") );

		fragment.appendChild(container);

		return fragment;
	}
}