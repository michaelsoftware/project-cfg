import UiHelper from '../../ui/UiHelper';
const StringUtils = require('../../../utils/StringUtils');
import NpmDependencyHelper from '../../../utils/NpmDependencies';

let info, name, version, author, main, keywords, license, homepage, privateProj, description;
let bugsUrl, bugsEmail;
let dependencies, devDependencies, peerDependencies, bundledDependencies, optionalDependencies;

export default class NpmUi {
	constructor() {
		this.initVars();
	}

	initVars() {
		info 		= document.querySelector('.project_info');
		name 		= document.querySelector('.project_name');
		version		= document.querySelector('.project_version');
		author 		= document.querySelector('.project_author');
		main 		= document.querySelector('.project_main');
		keywords	= document.querySelector('.project_keywords');
		license 	= document.querySelector('.project_license');
		homepage 	= document.querySelector('.project_homepage');
		privateProj = document.querySelector('.project_private');
		description = document.querySelector('.project_description');

		bugsUrl 	= document.querySelector('.project_bugs--url');
		bugsEmail 	= document.querySelector('.project_bugs--email');

		dependencies 			= document.querySelector('.project_dependencies');
		devDependencies 		= document.querySelector('.project_devDependencies');
		peerDependencies 		= document.querySelector('.project_peerDependencies');
		bundledDependencies 	= document.querySelector('.project_bundledDependencies');
		optionalDependencies 	= document.querySelector('.project_optionalDependencies');
	}

	load(npm, config) {
		info.innerText = config.path || '';
		name.value = npm.name ? npm.name : '';
		version.value = npm.version ? npm.version : '1.0.0';
		author.value = npm.author ? npm.author : '';
		main.value = npm.main ? npm.main : './src/index.js';
		keywords.value = npm.keywords ? npm.keywords.join(', ') : '';
		license.value = npm.license ? npm.license : '';
		homepage.value = npm.homepage ? npm.homepage : '';
		privateProj.checked = !!npm.private;
		description.value = npm.description ? npm.description : '';

		bugsUrl.value = this.getBugsUrl(npm);
		bugsEmail.value = (npm.bugs && npm.bugs.email) ? npm.bugs.email : '';

		const dependenciesList = NpmDependencyHelper.getFromConfig(npm);
		this.updateDependenciesList(dependenciesList.dependencies, dependencies, 'dependencies');
		this.updateDependenciesList(dependenciesList.devDependencies, devDependencies, 'devDependencies');
		this.updateDependenciesList(dependenciesList.peerDependencies, peerDependencies, 'peerDependencies');
		this.updateDependenciesList(dependenciesList.bundledDependencies, bundledDependencies, 'bundledDependencies');
		this.updateDependenciesList(dependenciesList.optionalDependencies, optionalDependencies, 'optionalDependencies');
	}

	save() {
		return ({
			name: name.value.trim(),
			version: version.value.trim(),
			author: author.value.trim(),
			main: main.value.trim(),
			keywords: getArray(keywords.value),
			license: license.value.trim(),
			homepage: homepage.value.trim(),
			private: privateProj.checked,
			description: description.value.trim(),
			bugs: this.getBugsConfig()
		});
	}

	getUi() {
		let fragment = document.createDocumentFragment();

		fragment.appendChild( UiHelper.getHeadline("NPM-Configuration") );
		fragment.appendChild( UiHelper.getKeyValue("Path:", "", "project_info") );
		fragment.appendChild( UiHelper.getKeyValueInput("Name:", "", "project_name") );
		fragment.appendChild( UiHelper.getKeyValueInput("Version:", "", "project_version") );
		fragment.appendChild( UiHelper.getKeyValueInput("Author:", "", "project_author") );
		fragment.appendChild( UiHelper.getKeyValueInput("Main:", "", "project_main") );
		fragment.appendChild( UiHelper.getKeyValueInput("Keywords:", "", "project_keywords") );
		fragment.appendChild( UiHelper.getKeyValueInput("License:", "", "project_license") );
		fragment.appendChild( UiHelper.getKeyValueInput("Homepage:", "", "project_homepage") );
		fragment.appendChild( UiHelper.getKeyValueElement("Bugs:", this.getBugsUi()) );
		fragment.appendChild( UiHelper.getCheckbox("Private package", "project_private") );
		fragment.appendChild( UiHelper.getKeyValueTextarea("Description:", "", "project_description") );


		fragment.appendChild( UiHelper.getHeadline("Dependencies", true) );
		fragment.appendChild( this.createDependencyElements() );

		return fragment;
	}

	createDependencyElements() {
		let fragment = document.createDocumentFragment();

		let dependencies = document.createElement('div');
		dependencies.className = 'project_dependencies';
		fragment.appendChild(UiHelper.getDetails("dependencies", dependencies));

		let devDependencies = document.createElement('div');
		devDependencies.className = 'project_devDependencies';
		fragment.appendChild(UiHelper.getDetails("devDependencies", devDependencies));

		let peerDependencies = document.createElement('div');
		peerDependencies.className = 'project_peerDependencies';
		fragment.appendChild(UiHelper.getDetails("peerDependencies", peerDependencies));

		let bundledDependencies = document.createElement('div');
		bundledDependencies.className = 'project_bundledDependencies';
		fragment.appendChild(UiHelper.getDetails("bundledDependencies", bundledDependencies));

		let optionalDependencies = document.createElement('div');
		optionalDependencies.className = 'project_optionalDependencies';
		fragment.appendChild(UiHelper.getDetails("optionalDependencies", optionalDependencies));

		return fragment;
	}

	getBugsUi() {
		let fragment = document.createDocumentFragment();

		fragment.appendChild(UiHelper.getInput("", "project_bugs--url", "Url"));
		fragment.appendChild(UiHelper.getInput("", "project_bugs--email", "Email"));

		return fragment;
	}

	getBugsConfig() {
		const url = trim(bugsUrl.value);
		const email = trim(bugsEmail.value);

		if(url && !email) {
			return url;
		}

		if(url && email) {
			return {
				url: url,
				email: email
			};
		}

		if(email) {
			return {
				email: email
			};
		}

		return "";
	}

	getBugsUrl(npm) {
		if(npm.bugs && npm.bugs.url) return npm.bugs.url;

		if(StringUtils.isString(npm.bugs)) return npm.bugs;

		return '';
	}

	updateDependenciesList(dependenciesList, node, dependenciesType) {
		let fragment = document.createDocumentFragment();

		if(dependenciesList)
		dependenciesList.map((dependency) => {
			fragment.appendChild( UiHelper.getKeyValue(dependency.name, dependency.version) );
		});

		node.innerHTML = '';

		if(!dependenciesList || dependenciesList.length === 0) {
			node.innerHTML = `<div style="text-align: center; font-weight: bold;">This project doesn\'t has any ${dependenciesType}.</div>`;
		}

		node.appendChild(fragment);
	}
}

function getArray(input) {
	let output = input.split(',');

	return output.map((element) => {
		return element.replace(/^\s+/, '').replace(/\s+$/, '');
	})
}

function trim(string) {

	return string;
}