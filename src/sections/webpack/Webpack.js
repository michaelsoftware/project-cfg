import UiHelper from '../../ui/UiHelper';


let enable, options, devServer, css, sass;

export default class WebpackUi {
	constructor() {
		this.initVars();
	}

	initVars() {
		enable = document.querySelector('.project_webpack');

		options = document.querySelector('.webpack__options');
		devServer = document.querySelector('.webpack_dev-server');
		css = document.querySelector('.webpack_css');
		sass = document.querySelector('.webpack_sass');

		this.updateDisplayOptions();

		if(enable)
			enable.onchange = this.updateDisplayOptions.bind(this);

		if(sass)
			sass.onchange = this.enableCss.bind(this);
	}

	updateDisplayOptions() {
		if(options)
			options.style.display = (enable.checked) ? 'block' : 'none';
	}

	enableCss() {
		css.checked = true;
	}

	load(config, globalConfig) {
		enable.checked = !!config.enabled;
		devServer.checked = !!config.devServer;
		css.checked = !!config.css;
		sass.checked = !!config.sass;

		this.updateDisplayOptions();
	}

	save() {
		return ({
			enabled: enable.checked,
			devServer: devServer.checked,
			css: css.checked,
			sass: sass.checked
		});
	}

	getUi() {
		let fragment = document.createDocumentFragment();

		fragment.appendChild( UiHelper.getHeadline("Webpack-Configuration") );
		fragment.appendChild( UiHelper.getCheckbox("Activate webpack V2", "project_webpack") );

		let container = document.createElement('div');
		container.className = 'webpack__options';

		container.appendChild( UiHelper.getHorizontalLine() );
		container.appendChild( UiHelper.getCheckbox("Activate webpack-dev-server", "webpack_dev-server") );
		container.appendChild( UiHelper.getCheckbox("Activate css/style-Loader", "webpack_css") );
		container.appendChild( UiHelper.getCheckbox("Activate sass-Loader", "webpack_sass") );

		fragment.appendChild(container);

		return fragment;
	}
}