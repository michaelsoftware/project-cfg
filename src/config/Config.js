export default class Config {
	static async load() {
		const response = await window.fetch(__DEV__ ? 'http://127.0.0.1:25565/api' : 'api');

		return response.json();
	}
}