import Config from './config/Config';
import NpmUi from './sections/npm/Npm';
import BabelUi from './sections/babel/Babel';
import WebpackUi from './sections/webpack/Webpack';

import UiHelper from './ui/UiHelper';

export default class App {
	constructor() {
		this.init();

		this.load();
	}

	async load() {
		window.spinner.loading();

		const config = await Config.load();

		this.npm.load(config.npm || {}, config);
		this.babel.load(config.babel || {}, config);
		this.webpack.load(config.webpack || {}, config);

		window.spinner.loaded();
	}

	async save() {
		window.spinner.loading('Project will be configured. This process might take some time.');

		const response = await fetch((__DEV__ ? 'http://127.0.0.1:25565/' : '') + 'api/save', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				npm: this.npm.save(),
				babel: this.babel.save(),
				webpack: this.webpack.save()
			})
		});

		const json = await response.json();

		this.npm.load(json.npm, json);

		window.spinner.loaded();
	}

	init() {
		this.initVars();
		this.initListeners();
	}

	initVars() {
		this.content = document.querySelector('div.content');

		this.npm = new NpmUi();
		UiHelper.createSection(this.content, this.npm.getUi());
		this.npm.initVars();

		this.babel = new BabelUi();
		UiHelper.createSection(this.content, this.babel.getUi());
		this.babel.initVars();

		this.webpack = new WebpackUi();
		UiHelper.createSection(this.content, this.webpack.getUi());
		this.webpack.initVars();


		let saveSection = document.createElement('section');
		saveSection.className = 'save';
		saveSection.appendChild( UiHelper.getButton('Save', this.save.bind(this), 'project_save') );
		this.content.appendChild(saveSection);

		this.saveButton = document.querySelector('.project_save');
	}

	initListeners() {
		this.saveButton.onclick = this.save.bind(this);
	}
}