let index = 0;


export default class UiHelper {
	static getHeadline(content, small) {
		let headline = document.createElement(small ? 'h2' : 'h1');
		headline.appendChild( document.createTextNode(content) );

		return headline;
	}

	static getKeyValue(key, value, className) {
		let wrapper = document.createElement('div');
		wrapper.className = "key-value--wrapper key-value--text";

		let keyElement = document.createElement('div');
		keyElement.className = "key-value--key";
		keyElement.appendChild( document.createTextNode(key) );


		let valueElement = document.createElement('div');
		valueElement.className = "key-value--value " + (className || '');
		valueElement.appendChild( document.createTextNode(value) );

		wrapper.appendChild(keyElement);
		wrapper.appendChild(valueElement);

		return wrapper;
	}

	static getKeyValueInput(key, value, className) {
		const idString = UiHelper.getIdString();

		let wrapper = document.createElement('div');
		wrapper.className = "key-value--wrapper key-value--input";

		let keyElement = document.createElement('label');
		keyElement.className = "key-value--key";
		keyElement.setAttribute("for", idString);
		keyElement.appendChild( document.createTextNode(key) );


		let valueElement = document.createElement('input');
		valueElement.setAttribute("type", "text");
		valueElement.className = "key-value--value " + (className || '');
		valueElement.value = value;
		valueElement.setAttribute("id", idString);

		wrapper.appendChild(keyElement);
		wrapper.appendChild(valueElement);

		return wrapper;
	}

	static getKeyValueSelect(key, values, className) {
		const idString = UiHelper.getIdString();

		let wrapper = document.createElement('div');
		wrapper.className = "key-value--wrapper key-value--select";

		let keyElement = document.createElement('label');
		keyElement.className = "key-value--key";
		keyElement.setAttribute("for", idString);
		keyElement.appendChild( document.createTextNode(key) );


		let valueElement = document.createElement('select');
		valueElement.setAttribute("type", "text");
		valueElement.className = "key-value--value " + (className || '');
		valueElement.setAttribute("id", idString);

		values.map((element) => {
			if(!element[1]) element[1] = element[0];

			let option = document.createElement('option');
			option.setAttribute("value", element[1]);
			option.appendChild( document.createTextNode(element[0]) );

			valueElement.appendChild(option);
		});

		wrapper.appendChild(keyElement);
		wrapper.appendChild(valueElement);

		return wrapper;
	}

	static getKeyValueTextarea(key, value, className) {
		const idString = UiHelper.getIdString();

		let wrapper = document.createElement('div');
		wrapper.className = "key-value--wrapper key-value--textarea";

		let keyElement = document.createElement('label');
		keyElement.className = "key-value--key";
		keyElement.setAttribute("for", idString);
		keyElement.appendChild( document.createTextNode(key) );


		let valueElement = document.createElement('textarea');
		valueElement.className = "key-value--value " + (className || '');
		valueElement.value = value;
		valueElement.setAttribute("id", idString);

		wrapper.appendChild(keyElement);
		wrapper.appendChild(valueElement);

		return wrapper;
	}

	static getKeyValueElement(key, node) {
		let wrapper = document.createElement('div');
		wrapper.className = "key-value--wrapper key-value--element";

		let keyElement = document.createElement('label');
		keyElement.className = "key-value--key";
		keyElement.appendChild( document.createTextNode(key) );


		let valueElement = document.createElement('div');
		valueElement.className = "key-value--value";
		valueElement.appendChild(node);

		wrapper.appendChild(keyElement);
		wrapper.appendChild(valueElement);

		return wrapper;
	}

	static getCheckbox(label, className) {
		const idString = UiHelper.getIdString();

		let wrapper = document.createElement('div');
		wrapper.className = "checkbox--wrapper";

		let checkbox = document.createElement('input');
		checkbox.setAttribute("type", "checkbox");
		checkbox.className = (className || '');
		checkbox.setAttribute("id", idString);

		let labelElement = document.createElement('label');
		labelElement.className = "key-value--key";
		labelElement.setAttribute("for", idString);
		labelElement.appendChild( document.createTextNode(label) );

		wrapper.appendChild(checkbox);
		wrapper.appendChild(labelElement);

		return wrapper;
	}

	static getButton(value, callback, className) {
		let button = document.createElement('button');
		button.innerText = value;
		button.className = className;

		if(callback && callback.call)
			button.onclick = callback;

		return button;
	}

	static getDetails(summary, node) {
		let fragment = document.createDocumentFragment();


		let details = document.createElement('details');

			let summaryElement = document.createElement('summary');
			summaryElement.appendChild( document.createTextNode(summary) );
			details.appendChild(summaryElement);

			details.appendChild(node);

		fragment.appendChild(details);


		return fragment;
	}

	static getInput(value, className, placeholder) {
		let input = document.createElement('input');
		input.setAttribute("type", "text");
		input.value = value;
		input.className = className;
		input.setAttribute("placeholder", placeholder);

		return input;
	}

	static getHorizontalLine() {
		return document.createElement('hr');
	}

	static createSection(root, node) {
		let section = document.createElement('section');
		section.appendChild(node);

		root.appendChild(section);
	}

	static getIdString() {
		return `custom_${index++}`;
	}
}