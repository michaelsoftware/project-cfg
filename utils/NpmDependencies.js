export default class NpmDependencies {
	static getFromConfig(npmConfig) {

		let retval = {};

		if(npmConfig.dependencies) {
			retval.dependencies = NpmDependencies.getPackages(npmConfig.dependencies)
		}

		if(npmConfig.devDependencies) {
			retval.devDependencies = NpmDependencies.getPackages(npmConfig.devDependencies)
		}

		if(npmConfig.peerDependencies) {
			retval.peerDependencies = NpmDependencies.getPackages(npmConfig.peerDependencies)
		}

		if(npmConfig.bundledDependencies) {
			retval.bundledDependencies = NpmDependencies.getPackages(npmConfig.bundledDependencies)
		}

		if(npmConfig.optionalDependencies) {
			retval.optionalDependencies = NpmDependencies.getPackages(npmConfig.optionalDependencies)
		}

		return retval;
	}

	static getPackages(packageObject) {
		let returnArray = [];

		for(const key in packageObject) {
			if(!packageObject.hasOwnProperty(key)) continue;

			returnArray.push({
				name: key,
				version: packageObject[key]
			});
		}

		return returnArray;
	}
}