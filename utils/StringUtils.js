module.exports = class StringUtils {
	static removeLast(str) {
		if (str != null && str.length > 0) {
			str = str.substring(0, str.length-1);
		}
		return str;
	}

	static isString(object) {
		return (typeof object === 'string' || object instanceof String);
	}
};