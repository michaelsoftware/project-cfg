[![npm](https://img.shields.io/npm/v/project-cfg.svg?style=flat-square)](https://www.npmjs.com/package/project-cfg/)
[![npm](https://img.shields.io/npm/l/project-cfg.svg?style=flat-square)](https://gitlab.com/michaelsoftware/project-cfg/blob/master/LICENSE)

# project-cfg #
This package helps you to configure your babel/webpack-project.
You should install it globally using:
```text
    npm install -g project-cfg
```

## Usage ##
**Please make a backup of your config-files
(package.json, .babelrc and webpack) first.**

You can use it in your own project with:
```text
    project-cfg
```

It will start an express server and let you configure
your project using a webinterface.

Make your changes and apply them with 'save'. This process will take
some time, because project-cfg will install all necessary dependencies
for you.

_You should use a modern browser, because the webinterface
use some ES6-functions._