#!/usr/bin/env node

const index = require('../lib/index.js');


const default_port = 25565;  // port 25565 normally not used in web-development

index.startServer(default_port);
